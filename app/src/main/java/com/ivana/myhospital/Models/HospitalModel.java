package com.ivana.myhospital.Models;



/**
 * Created by Ivana on 09/05/2018.
 */

public class HospitalModel {
    public String name;
    public String street;
    public String website;
    public String telephone;
    public String image;
    public String latitude;
    public String longitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public HospitalModel(String name, String street, String website, String telephone, String image, String latitude, String longitude) {
        this.name = name;
        this.street = street;
        this.website = website;
        this.telephone = telephone;
        this.image = image;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public HospitalModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}

package com.ivana.myhospital;

import android.app.Activity;
import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

public class HospitalDetailsActivity extends AppCompatActivity {
    public Activity activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_details);
        Intent intent = getIntent();
        final String ime = intent.getStringExtra("name");
        String slika = intent.getStringExtra("image");
        String telefon = intent.getStringExtra("telephone");
        String strana = intent.getStringExtra("website");
        String ulica = intent.getStringExtra("street");
        final String latitude = intent.getStringExtra("latitude");
        final String longitude = intent.getStringExtra("longitude");
        TextView name = (TextView) findViewById(R.id.ime);
        TextView street = (TextView) findViewById(R.id.street);
        TextView telephone = (TextView) findViewById(R.id.telephone);
        ImageView image = (ImageView) findViewById(R.id.image);
        TextView website = (TextView) findViewById(R.id.website);
        AppCompatButton button = (AppCompatButton) findViewById(R.id.location);
        name.setText("Име на болница: "+ime);
        Picasso.with(getApplicationContext()).load(slika).into(image);
        street.setText("Улица: "+ulica);
        telephone.setText("Телефон: "+telefon);
        website.setText("Website: "+strana);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(HospitalDetailsActivity.this,MapsActivity.class);
                intent1.putExtra("name",ime);
                intent1.putExtra("latitude",latitude);
                intent1.putExtra("longitude",longitude);
                getApplicationContext().startActivity(intent1);
            }
        });








    }
}

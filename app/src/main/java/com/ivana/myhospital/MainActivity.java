package com.ivana.myhospital;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ivana.myhospital.Adapters.GetHospitals;
import com.ivana.myhospital.Adapters.HospitalAdapter;
import com.ivana.myhospital.Models.HospitalModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {
    public RecyclerView recyclerView;
    public HospitalAdapter adapter;
    public ArrayList<HospitalModel> list;
    String URL_DATA="https://pastebin.com/raw/B9NQzuU3";
    String response=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        CheckConnection();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        GetHospitals getHospitals = new GetHospitals();
        try {
            response = getHospitals.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        list = GetData(response);
        adapter = new HospitalAdapter(list,this,getApplicationContext());
        DividerItemDecoration decoration = new DividerItemDecoration(getApplicationContext(),DividerItemDecoration.HORIZONTAL);
        recyclerView.addItemDecoration(decoration);
        recyclerView.setAdapter(adapter);
        recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,HospitalDetailsActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });



    }

    public ArrayList<HospitalModel> GetData(String result)
    {
        ArrayList<HospitalModel> list = new ArrayList<>();

        try {
            JSONObject object = new JSONObject(result.toString());
            JSONArray array = object.getJSONArray("hospitals");
            for(int i=0;i<array.length();i++)
            {
                String ime = array.getJSONObject(i).getString("name");
                String ulica = array.getJSONObject(i).getString("street");
                String telefon = array.getJSONObject(i).getString("telephone");
                String strana = array.getJSONObject(i).getString("website");
                String slika = array.getJSONObject(i).getString("image");
                String latitude = array.getJSONObject(i).getString("latitude");
                String longitude = array.getJSONObject(i).getString("longitude");
                HospitalModel model = new HospitalModel(ime,ulica,strana,telefon,slika,latitude,longitude);
                list.add(model);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }
    public void init()
    {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        list = new ArrayList<>();
        adapter = new HospitalAdapter(list,this,this.getApplicationContext());
        recyclerView.setAdapter(adapter);

    }
    public void CheckConnection()
    {
            if(!isConnected())
            {
                Toast.makeText(getApplicationContext(),"Connect to Internet",Toast.LENGTH_SHORT).show();
            }

    }
    public boolean isConnected()
    {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(manager.getActiveNetworkInfo()!=null && manager.getActiveNetworkInfo().isConnected())
        {
            return true;

        }
        else{

            return false;
        }


    }


}


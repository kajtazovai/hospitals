package com.ivana.myhospital.Adapters;


import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;


public class GetHospitals extends AsyncTask<Void,Void,String> {
    public String URL_DATA = "https://pastebin.com/raw/LYnspu3w";



    @Override
    protected String doInBackground(Void... voids) {
        String result = null;
        StringBuilder response = new StringBuilder();

        URL url ;
        try {

            url = new URL(URL_DATA);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()), 8129);
                String linija = null;
                while ((linija = br.readLine()) != null) {
                    response.append(linija);
                }
                br.close();
            }
        } catch (ProtocolException e1) {
            e1.printStackTrace();
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        result = response.toString();


       return result;
    }
}

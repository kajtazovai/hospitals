package com.ivana.myhospital.Adapters;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ivana.myhospital.HospitalDetailsActivity;
import com.ivana.myhospital.MainActivity;
import com.ivana.myhospital.Models.HospitalModel;
import com.ivana.myhospital.R;

import java.util.ArrayList;

/**
 * Created by Ivana on 14/05/2018.
 */

public class HospitalAdapter extends RecyclerView.Adapter<HospitalAdapter.HospitalViewHolder> {
    private ArrayList<HospitalModel> hospitals = new ArrayList<>();
    private Activity activity;
    Context context;


    public HospitalAdapter(ArrayList<HospitalModel> list,Activity activity,Context context) {
        hospitals = list;
        this.activity = activity;
        this.context = context;
    }


    @Override
    public HospitalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hospital_details,parent,false);

        return new HospitalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HospitalViewHolder holder, int position) {
        HospitalModel model = hospitals.get(position);
        Glide.with(activity).load(model.getImage()).into(holder.image);
        holder.name.setText(model.getName());



    }

    @Override
    public int getItemCount() {
        return hospitals.size();
    }
    public class HospitalViewHolder extends RecyclerView.ViewHolder {

        public ImageView image;
        public TextView street;
        public TextView telephone;
        public TextView name;
        public TextView website;


        public HospitalViewHolder(View itemView) {
            super(itemView);
            image =(ImageView) itemView.findViewById(R.id.image);
            street = (TextView) itemView.findViewById(R.id.street);
            telephone = (TextView) itemView.findViewById(R.id.telephone);
            website = (TextView) itemView.findViewById(R.id.website);
            name = (TextView) itemView.findViewById(R.id.name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int index = getAdapterPosition();
                    String ime = hospitals.get(index).getName();
                    String ulica = hospitals.get(index).getStreet();
                    String strana = hospitals.get(index).getWebsite();
                    String telefon = hospitals.get(index).getTelephone();
                    String slika = hospitals.get(index).getImage();
                    String latitude = hospitals.get(index).getLatitude();
                    String longitude = hospitals.get(index).getLongitude();
                    Intent intent = new Intent(context.getApplicationContext(), HospitalDetailsActivity.class);
                    intent.putExtra("name",ime);
                    intent.putExtra("street",ulica);
                    intent.putExtra("telephone",telefon);
                    intent.putExtra("image",slika);
                    intent.putExtra("website",strana);
                    intent.putExtra("latitude",latitude);
                    intent.putExtra("longitude",longitude);
                    context.startActivity(intent);
                }
            });
        }
        public void prikazi(ArrayList<HospitalModel> list)
        {
            hospitals = list;

        }


    }
}
